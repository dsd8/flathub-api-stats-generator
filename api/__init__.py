"""
Copyright (C) 2018, 2019
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime
import json
from os import makedirs, path
import requests


# If we can't use blake2 fallback to md5
# we don't need anything secure as this is just for caching paths
try:
    from hashlib import blake2b

    def hash_hexdigest(data):
        return blake2b(data, digest_size=8).hexdigest()
except ImportError:
    from hashlib import md5

    def hash_hexdigest(data):
        return md5(data).hexdigest()


class RawAPI:
    @staticmethod
    def get_json_data(base, url, auth=[]):
        response = requests.get(path.join(base, url), auth=auth)

        if response.ok:
            return json.loads(response.content.decode("utf-8"))
        else:
            response.raise_for_status()
            return []


class CacheProxyAPI(RawAPI):
    CACHE_DIR = None

    @classmethod
    def get_json_data(cls, base, url, auth=[], can_cache=False):
        # If a CACHE_DIR is set then before querying online try in
        # the local cache directory
        #
        # Uses the folder structure CACHE_DIR/NAME@blake2b(base)/url
        # examples of the files created could include
        # /tmp/cache/Flathub@378931920ac0b738/2019/01/01.json
        if cls.CACHE_DIR is not None and can_cache is True:
            file_name = path.join(
                cls.CACHE_DIR or "",
                "%s@%s" % (cls.NAME, hash_hexdigest(base.encode("utf-8"))),
                url)

            if path.exists(file_name):
                with open(file_name, "rb") as f:
                    return json.loads(f.read().decode("utf-8"))
            else:
                data = RawAPI.get_json_data(base, url, auth=auth)
                makedirs(path.dirname(file_name), exist_ok=True)

                with open(file_name, "wb") as f:
                    f.write(json.dumps(data).encode("utf-8"))

                return data
        else:
            return RawAPI.get_json_data(base, url, auth=auth)


class BaseAPI(CacheProxyAPI):
    BASE_URL = ""
    DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
    FIELD_CREATION_DATE = ""
    FIELD_ID = ""
    NAME = "Unknown"

    @staticmethod
    def check(extra):
        pass

    @classmethod
    def get_apps(cls):
        return []

    @classmethod
    def get_app_by_id(cls, app_id):
        return {}

    @classmethod
    def get_app_by_record(cls, record):
        return {}

    @classmethod
    def get_downloads_for_date(cls, year="", month="", day=""):
        return {}

    @classmethod
    def parse_date(cls, date):
        formats = cls.DATETIME_FORMAT

        if type(formats) is not list:
            formats = [formats]

        for f in formats:
            try:
                return datetime.strptime(date, f)
            except ValueError:
                continue

        raise ValueError("time data %s does not match formats %s"
                         % (date, formats))


from .flathub import FlathubAPIv1
from .github import GithubFlathubAPI
