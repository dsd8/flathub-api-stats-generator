"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport, IS_RUNTIME
from model import DownloadModel


class DownloadsOnlyRuntimeByDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "app-id":
                print("downloads_only_runtimes does not support app-id filtering")
            else:
                print("Unknown report-arg %s=%s" % (key, value))

        # Pre read all the runtimes
        self.runtimes = []

        for _, data in self.model.downloads_by_date():
            for runtime in data["refs"]:
                if runtime not in self.runtimes and self.is_runtime_name(runtime):
                    self.runtimes.append(runtime)

    def runtime_count(self, data, index=0):
        return sum(
            sum(data["refs"][runtime][arch][index] for arch in data["refs"][runtime])
            for runtime in data["refs"] if runtime in self.runtimes
        )

    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_string(self):
        return (
            "%s\t%i\t%i" % (date, self.runtime_count(data, 0), self.runtime_count(data, 1))
            for date, data in self.model.downloads_by_date()
        )

    def data_zipped(self):
        return [
            zip(
                *(
                    [date, self.runtime_count(data, 0)]
                    for date, data in self.model.downloads_by_date()
                )
            ),
            zip(
                *(
                    [date, self.runtime_count(data, 1)]
                    for date, data in self.model.downloads_by_date()
                )
            )
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "x_date": True,
                "label": "Downloads",
            },
            {
                "x_date": True,
                "label": "Delta Downloads",
            }
        ]

    @staticmethod
    def is_runtime_name(name):
        return IS_RUNTIME.match(name)

    @property
    def title(self):
        return (
            "Downloads (only runtimes) in %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
