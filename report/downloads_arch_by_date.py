"""
Copyright (C) 2018, 2020
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import DownloadModel


class DownloadsArchByDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        # Pre read all the apps and arches
        self.arches = []
        self.apps = []
        self.exclude_arches = []

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "app-id":
                self.apps.append(value)
            elif key == "exclude-arch":
                self.exclude_arches.append(value)
            else:
                print("Unknown report-arg %s=%s" % (key, value))

        if len(self.apps) == 0:
            # Pre read all the apps
            for _, data in self.model.downloads_by_date():
                for app in data["refs"]:
                    if app not in self.apps:
                        self.apps.append(app)

        for _, data in self.model.downloads_by_date():
            for app in self.apps:
                for arch in data["refs"].get(app, []):
                    if arch not in self.arches and arch not in self.exclude_arches:
                        self.arches.append(arch)

    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_string(self):
        return [
            "%s\t%i" % (
                arch,
                sum(
                  sum(data["refs"].get(app, {}).get(arch, [0])[0] for app in self.apps)
                  for date, data in self.model.downloads_by_date()
                ),
            )
            for arch in self.arches
        ]

    def data_zipped(self):
        return [
            zip(
                *(
                    [date, sum(data["refs"].get(app, {}).get(arch, [0])[0] for app in self.apps)]
                    for date, data in self.model.downloads_by_date()
                )
            ) for arch in self.arches
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "label": arch,
                "x_date": True,
            } for arch in self.arches
        ]

    @property
    def title(self):
        return (
            "Downloads by architecture %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
